packer {
  required_plugins {
    ansible = {
      version = "~> 1"
      source = "github.com/hashicorp/ansible"
    }
    amazon = {
      source = "github.com/hashicorp/amazon"
      version = "~> 1"
    }
  }
}

variable "associate_public_ip_address" {
  type = string
  default = "true"
}

variable "base_ami" {
  type = string
  default = "ami-0c7217cdde317cfec"
}

variable "instance_type" {
  type = string
  default = "t3.medium"
}

variable "region" {
  type = string
  default = "us-east-1"
}

variable "app_name" {
  type = string
  default = "WebApp"
}
variable "ssh_username" {
  type = string
  default = "ubuntu"
}

locals {
  timestamp = formatdate("DD_MM_YYYY-hh_mm", timestamp())
}

source "amazon-ebs" "static-web-ami" {
  ami_name = "${var.app_name}-${local.timestamp}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  instance_type = "${var.instance_type}"
  region = "${var.region}"
  source_ami = "${var.base_ami}"
  ssh_username = "${var.ssh_username}"
  iam_instance_profile = "LabInstanceProfile"
  vpc_id = "vpc-06efc2f93d002b579"
  subnet_id = "subnet-0b5440a10ac31e906"

  tags = {
    Name = "${var.app_name}"
  }

}

build {
  sources = ["source.amazon-ebs.static-web-ami"]
  provisioner "ansible" {
    playbook_file = "./playbook/play.yml"
  }
}

